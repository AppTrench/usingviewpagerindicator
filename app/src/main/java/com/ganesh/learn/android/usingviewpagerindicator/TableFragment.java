package com.ganesh.learn.android.usingviewpagerindicator;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Ganesh on 22-04-2015.
 */
public class TableFragment extends Fragment {

    static Object[][] data = {
            {MainActivity.dateFormat.format(new Date(115, Calendar.JANUARY, 1)), 2050.00, 14.0f, 500.00f},
            {MainActivity.dateFormat.format(new Date(115, Calendar.FEBRUARY, 1)), 3550.00, 10.0f, 1000.00f},
            {MainActivity.dateFormat.format(new Date(115, Calendar.MARCH, 1)), 4350.00, 12.0f, 800.00f},
            {MainActivity.dateFormat.format(new Date(115, Calendar.APRIL, 1)), 5550.00, 13.0f, 900.00f}
    };

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.table_fragment, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();

        insertDataRows();
    }

    private void insertDataRows() {
        TableLayout layout = (TableLayout) getActivity().findViewById(R.id.dataTable);
        layout.removeViews(1, layout.getChildCount() - 1);

        for (int i = 0; i < 4; i++) {
            TableRow row = new TableRow(getActivity());
            TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
            row.setLayoutParams(layoutParams);

            for (int j = 0; j < 4; j++) {
                TextView textView = new TextView(getActivity());
                textView.setText(data[i][j].toString());
                row.addView(textView);
            }
            layout.addView(row);
        }
    }
}
