package com.ganesh.learn.android.usingviewpagerindicator;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;


public class BarChartFragment extends Fragment {

    public BarChartFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.bar_chart, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        setupLineChart();
    }

    private void setupLineChart() {
        BarChart chart = (BarChart) getActivity().findViewById(R.id.bar_chart);

        ArrayList<String> xVals = new ArrayList<>();
        ArrayList<BarEntry> entries = new ArrayList<>();
        setDataValues(xVals, entries);

        BarDataSet barDataSet = new BarDataSet(entries, "Mileage");
        barDataSet.setColor(getResources().getColor(R.color.blue));
        BarData lineData = new BarData(xVals, barDataSet);
        chart.setData(lineData);
        chart.setDescription("");

        final XAxis xAxis = chart.getXAxis();
        xAxis.setTextSize(13);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        chart.getBarData().setValueTextSize(13);

        barDataSet.setBarSpacePercent(75f);
        chart.getAxisLeft().setTextSize(13);
        chart.getAxisRight().setEnabled(false);
        chart.invalidate();
    }

    private void setDataValues(ArrayList<String> xVals, ArrayList<BarEntry> entries) {
        for (int i = 0; i < 4; i++) {
            xVals.add(TableFragment.data[i][0].toString());
            entries.add(new BarEntry((float) TableFragment.data[i][2], i));
        }
    }
}
