package com.ganesh.learn.android.usingviewpagerindicator;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by Ganesh on 15-04-2015.
 */
public class GraphsPagerAdapter extends FragmentPagerAdapter {

    public GraphsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new BarChartFragment();
            case 1:
                return new LineChartFragment();
            case 2:
                return new PieChartFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
