package com.ganesh.learn.android.usingviewpagerindicator;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;

import java.util.ArrayList;


public class PieChartFragment extends Fragment {
    public PieChartFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.pie_chart, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        setupLineChart();
    }

    private void setupLineChart() {
        PieChart chart = (PieChart) getActivity().findViewById(R.id.pie_chart);

        ArrayList<String> xVals = new ArrayList<>();
        ArrayList<Entry> entries = new ArrayList<>();
        setDataValues(xVals, entries);

        PieDataSet barDataSet = new PieDataSet(entries, "Expenses");
        barDataSet.setColors(new int[]{getResources().getColor(R.color.blue_page), getResources().getColor(R.color.red), getResources().getColor(R.color.yellow)});
        PieData lineData = new PieData(xVals, barDataSet);
        chart.setData(lineData);
        chart.setDescription("");

        chart.getData().setValueTextSize(13);
        chart.invalidate();
    }

    private void setDataValues(ArrayList<String> xVals, ArrayList<Entry> entries) {
        xVals.add("Fuel");
        entries.add(new Entry((float) 500.00, 0));

        xVals.add("Food");
        entries.add(new Entry((float) 100.0, 1));

        xVals.add("Clothes");
        entries.add(new Entry((float) 1200.0, 2));

    }
}
