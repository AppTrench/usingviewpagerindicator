package com.ganesh.learn.android.usingviewpagerindicator;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;


public class LineChartFragment extends Fragment {

    public LineChartFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.line_chart, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        setupLineChart();
    }

    private void setupLineChart() {
        LineChart chart = (LineChart) getActivity().findViewById(R.id.line_chart);

        ArrayList<String> xVals = new ArrayList<>();
        ArrayList<Entry> entries = new ArrayList<>();
        setDataValues(xVals, entries);

        LineDataSet lineDataSet = new LineDataSet(entries, "Expenses");
        lineDataSet.setColor(getResources().getColor(R.color.blue));
        LineData lineData = new LineData(xVals, lineDataSet);
        chart.setData(lineData);
        chart.setDescription("");

        final XAxis xAxis = chart.getXAxis();
        xAxis.setTextSize(13);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        chart.getLineData().setValueTextSize(13);

        chart.getAxisLeft().setTextSize(13);
        chart.getAxisRight().setEnabled(false);
        chart.invalidate();
    }

    private void setDataValues(ArrayList<String> xVals, ArrayList<Entry> entries) {
        for (int i = 0; i < 4; i++) {
            xVals.add(TableFragment.data[i][0].toString());
            entries.add(new BarEntry((float) TableFragment.data[i][3], i));
        }
    }
}
